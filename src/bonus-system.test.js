import { calculateBonuses as calc } from "./bonus-system";
require("assert");
const tiers = {
  standard: { name: "Standard", multiplier: 0.05 },
  premium: { name: "Premium", multiplier: 0.1 },
  diamond: { name: "Diamond", multiplier: 0.2 },
  zero: { name: "ZERO", multiplier: 0 },
};

const bellow10k = [
  {
    amount: 1000,
    bonus: 1,
  },
  {
    amount: 9999,
    bonus: 1,
  },
  {
    amount: 0,
    bonus: 1,
  },
  {
    amount: -42,
    bonus: 1,
  },
  {
    amount: -100500,
    bonus: 1,
  },
];

const bellow50k = [
  {
    amount: 10000,
    bonus: 1.5,
  },
  {
    amount: 49999,
    bonus: 1.5,
  },
];

const bellow100k = [
  {
    amount: 99999,
    bonus: 2,
  },
  {
    amount: 50000,
    bonus: 2,
  },
];

const superBonus = [
  {
    amount: 100500,
    bonus: 2.5,
  },
  {
    amount: 100000,
    bonus: 2.5,
  },
];

describe("Standard bonus system tests start", () => {
  console.log("Tests started");

  test("programme : Standard, amount : x < 10k", (done) => {
    const values = bellow10k;
    const { name, multiplier } = tiers.standard;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier);
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier);
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier);

    done();
  });

  test("programme : Standard, amount : x < 50k", (done) => {
    const values = bellow50k;
    const { name, multiplier } = tiers.standard;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : Standard, amount : x < 100k", (done) => {
    const values = bellow100k;
    const { name, multiplier } = tiers.standard;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : Standard, amount : x > 100k", (done) => {
    const values = superBonus;
    const { name, multiplier } = tiers.standard;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });
});

describe("Premium bonus system tests start", () => {
  console.log("Premium started");
  test("programme : Premium, amount : x < 10k", (done) => {
    const values = bellow10k;
    const { name, multiplier } = tiers.premium;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier);
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier);
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier);

    done();
  });

  test("programme : Premium, amount : x < 50k", (done) => {
    const values = bellow50k;
    const { name, multiplier } = tiers.premium;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : Premium, amount : x < 100k", (done) => {
    const values = bellow100k;
    const { name, multiplier } = tiers.premium;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : Premium, amount : x > 100k", (done) => {
    const values = superBonus;
    const { name, multiplier } = tiers.premium;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });
});

describe("Diamond bonus system tests start", () => {
  console.log("Premium started");
  test("programme : Diamond, amount : x < 10k", (done) => {
    const values = bellow10k;
    const { name, multiplier } = tiers.diamond;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier);
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier);
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier);

    done();
  });

  test("programme : Diamond, amount : x < 50k", (done) => {
    const values = bellow50k;
    const { name, multiplier } = tiers.diamond;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : Diamond, amount : x < 100k", (done) => {
    const values = bellow100k;
    const { name, multiplier } = tiers.diamond;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : Diamond, amount : x > 100k", (done) => {
    const values = superBonus;
    const { name, multiplier } = tiers.diamond;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });
});

describe("ZERO bonus system tests start", () => {
  console.log("ZERO bonus started");
  test("programme : ZERO, amount : x < 10k", (done) => {
    const values = bellow10k;
    const { name, multiplier } = tiers.zero;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);
    expect(calc(name, values[2].amount)).toEqual(values[2].bonus * multiplier);
    expect(calc(name, values[3].amount)).toEqual(values[3].bonus * multiplier);
    expect(calc(name, values[4].amount)).toEqual(values[4].bonus * multiplier);

    done();
  });

  test("programme : ZERO, amount : x < 50k", (done) => {
    const values = bellow50k;
    const { name, multiplier } = tiers.zero;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : ZERO, amount : x < 100k", (done) => {
    const values = bellow100k;
    const { name, multiplier } = tiers.zero;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });

  test("programme : ZERO, amount : x > 100k", (done) => {
    const values = superBonus;
    const { name, multiplier } = tiers.zero;

    expect(calc(name, values[0].amount)).toEqual(values[0].bonus * multiplier);
    expect(calc(name, values[1].amount)).toEqual(values[1].bonus * multiplier);

    done();
  });
});
